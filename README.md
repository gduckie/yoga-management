1. Schedule:

Đức:
```
	+ Thời khóa biểu
	+ Hóa đơn
	+ Gói tập
	+ Thẻ học viên
	+ Học viên
	+ Học phí
```

Long + Quỳnh: 
```
	+ Ca: quản lý ca học( thêm sửa xóa)
	+ giáo viên: ( thêm sửa xóa)
	+ Dụng cụ: ( thêm sửa xóa)
	+ Môn học: ( thêm sửa xóa)
	+ Phòng: ( thêm sửa xóa)
```

**Lưu ý:** 

- Xử lý sự kiện đóng, mở connection: sử dụng lớp Database
- Long và Quỳnh sẽ sử dụng table bootstrap: https://getbootstrap.com/docs/4.4/content/tables/ cho phần UI cũng như xử lý sự kiện, bắt lỗi input ở các phần quản lý.
- Long và Quỳnh code theo các file .aspx tui đã tạo sẵn, không cần tạo file mới.
- Import file exported_SQL to use database

2. Cách sử dụng lớp clsDatabase(helper):
 - clsDatabase Element: trả về đối tượng lớp clsDatabase.
 - DataTable Load(string sql): return DataTable (see docs in ASP.NET), sử dụng khi query bảng
    ```
    string query = "select distinct s.id as id, s.name as name " +
                "from subject as s " +
                "join course as c ON s.id = c.subject_id " +
                "where c.teacher_id = '" + this.teacher_id + "';";
            DataTable data = clsDatabase.Element.Load(query);
            if(data.Rows.Count > 0)
            {
                foreach(DataRow row in data.Rows)
                {
                    Button b = new Button();
                    b.Text = row["id"].ToString() + " " +
                        row["name"].ToString();
                    
                    b.Click += new EventHandler(c_click);
                    b.AutoSize = true;
                    b.Name = row["id"].ToString();
                    container.Controls.Add(b);
                    i += 1;
                }
            }
    
    ```
 - int Execute(string sql): sử dụng khi insert, update hoặc delete
    ```
    try
        {
            string query = " UPDATE point SET value = '" + p + "' " +
                "WHERE student_id = '" + sid + "' AND course_id = '" + cid + "';";
            clsDatabase.Element.Execute(query);
            MessageBox.Show("success updated");
        }
    catch(Exception ex)
        {
            MessageBox.Show("failed updated" + ex);
        }
    ```
 - List<string> Read(string sql): sử dụng để query câu lệnh SELECT
    ```
        EX:
        string query = "SELECT * FROM teacher WHERE id = '" + maCanBo.Text.Trim() + "' AND password = '" + matKhau.Text.Trim() + "'";
            List<string> data = clsDatabase.Element.Read(query);
            if (data.Count > 0)
            {
                Home home = new Home(maCanBo.Text, data[0]);
                home.Visible = true;
                this.Visible = false;
            }
            else
            {
                MessageBox.Show("Incorrect username or password");
            }
    ``` 
