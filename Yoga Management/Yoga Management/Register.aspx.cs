﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Yoga_Management
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtPwd.CssClass = "form-control form-control-user";
            txtRePwd.CssClass = "form-control form-control-user";
            txtErrorPwd.Text = "";
            txtErrorRePwd.Text = "";
        }

        private bool isNullData(DataTable data)
        {
                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        if (row["HV_ID"].ToString() != "")
                        {
                            return false;
                        }
                    }
                }
            return true;
        }
        private void setErrorNull()
        {
            txtErrorEmail.Text = "";
            txtErrorRePwd.Text = "";
            txtErrorPwd.Text = "";
        }
        protected void btnRegister_Click(object sender, EventArgs e)
        {            
            try
            {
                setErrorNull();
                string queryUser = "SELECT * FROM HOCVIEN WHERE HV_UNAME = '" + txtEmail.Text.Trim() + "'";
                DataTable userData = clsDatabase.Element.Load(queryUser);
                if (!errorPhone.IsValid)
                {
                    txtSDT.CssClass += " border-danger";
                }
                if(txtPwd.Text != txtRePwd.Text)
                {
                    txtPwd.CssClass += " border-danger";
                    txtRePwd.CssClass += " border-danger";
                    txtErrorPwd.Text = "Password is not match, please try again!";
                    txtErrorRePwd.Text = "Password is not match, please try again!";
                    return;
                }
                if (!isNullData(userData))
                {
                    txtEmail.CssClass += " border-danger";
                    txtErrorEmail.Text = "This email is existed, please try another email!";
                    return;
                }
                string query = "INSERT INTO HOCVIEN(HV_TEN, HV_NAMSINH, HV_SDT, HV_UNAME, HV_PWD) values  (N'" + txtName.Text + "','" + txtNamSinh.Text +
                            "','" + txtSDT.Text + "','" + txtEmail.Text + "','" + txtPwd.Text + "')";
                

                clsDatabase.Element.Execute(query);
                Response.Write("<script>alert('welcome!')</script>");
                Response.Redirect("/Login.aspx");
            }
            catch(Exception ex)
            {
                Response.Write(ex.ToString());
            }
            

        }
    }
}