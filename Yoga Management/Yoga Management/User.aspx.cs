﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Yoga_Management
{
    public partial class User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["uname"] != null)
            {
                txtNameUser.Text = Session["uname"].ToString();
            }
            
        }
        protected void fetchLogOut(object sender, EventArgs e)
        {
            Session.Remove("role");
            Session.Remove("admin-id");
            Response.Redirect("/Login.aspx");
        }
    }
}