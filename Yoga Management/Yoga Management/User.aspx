﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="Yoga_Management.User" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User page</title>
    
    <link href="css/bootstrap-bss.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
     <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap-bss.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="js/theme.js"></script>
    <style type="text/css">
        .testimonials {
          padding-top: 7rem;
          padding-bottom: 7rem;
        }
        .features-icons {
          padding-top: 7rem;
          padding-bottom: 7rem;
        }
        .btn-primary {
            color: #fff;
            background-color: #6832a8 !important;
            border-color: #6832a8 !important;
        }
        .text-main {
            color: #6832a8 !important;
        }

        .text-primary {
            color: #b393c7 !important;
        }

        header.masthead .overlay {
          position: absolute;
          background-color: #212529;
          height: 100%;
          width: 100%;
          top: 0;
          left: 0;
          opacity: .3;
        }
        @media (min-width:768px) {
          header.masthead {
            padding-top: 12rem;
            padding-bottom: 12rem;
          }
        }

        header.masthead {
          position: relative;
          background-color: #343a40;
          background-size: cover;
          padding-top: 8rem;
          padding-bottom: 8rem;
        }



    </style>
</head>
<body>
    

    <form id="form1" runat="server">
        <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-light" id="mainNav" style="border: 1px solid #d9d9d9;">
            <div class="container"><a class="navbar-brand" href="#page-top"><img class="rounded-circle" src="img/chakra.png" style="height: 32px;width: 32px;" /> Siva Yoga</a><button data-toggle="collapse" data-target="#navbarResponsive" class="navbar-toggler navbar-toggler-right" type="button"
                    data-toogle="collapse" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="nav navbar-nav ml-auto text-uppercase">
                        <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="/User">Home</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="#introduction">about</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="#package">Package</a></li>
                        <% 
                            if (Session["uname"] == null)
                            {
                        %>
                        <li class="nav-item" role="presentation"><a class="btn btn-primary" href="/Login.aspx" style="background-color: #6832a8 !important;">Sign in</a></li>
                        <%} %>
                        
                        <% 
                            if (Session["uname"] != null)
                            {
                        %>
                        <li role="presentation" class="nav-item dropdown no-arrow">
                            <div class="nav-item dropdown no-arrow">
                                 <a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#"> 
                                      <span class="d-none d-lg-inline mr-2 text-gray-600 small">
                                          <asp:Label ID="txtNameUser" runat="server" Text=""></asp:Label></span>
                                 </a>
                                  <div role="menu" class="dropdown-menu shadow dropdown-menu-right animated--grow-in"><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-icon"></i> Profile</a><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-icon"></i> Cài đặt</a>
                                                <div class="dropdown-divider"></div>
                        
                                            <a role="presentation" class="dropdown-item" href="#">
                                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-icon"></i> 
                                                    <asp:Button ID="btnLogout" CssClass="btn btn-link" runat="server" Text="Log out" OnClick="fetchLogOut" /></a>

                                  </div>
                            </div>
                        </li>
                        <%} %>
                        
                    </ul>
                </div>
            </div>
        </nav>
        <div class="dc-test"></div>
        <header id="home" class="masthead text-white text-center" style="background: url('img/imgHome.jpg')no-repeat center center;background-size: cover;background-image: url('img/imgHome.jpg');height: 800px;">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <h1 class="mb-5">For peace of mind, a better body, and sanity in a hectic world with yoga<br /></h1>
                    </div>
                    <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                    
                            <div class="form-row">
                                <div class="col-12 col-md-9 mb-2 mb-md-0"><input class="form-control form-control-lg" type="email" placeholder="Enter your email to register..." /></div>
                                <div class="col-12 col-md-3"><button class="btn btn-primary btn-block btn-lg" type="button" data-toggle="modal" data-target="#signup">Register!</button></div>
                            </div>
                    
                    </div>
                </div>
            </div>
        </header>
    
        <section id="introduction" class="features-icons bg-light text-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6"><img class="rounded-circle" src="img/imgAd.jpg" style="width: 100%;" /></div>
                    <div class="col-xl-6" style="margin: 0 auto;">
                        <h3 style="margin-top: 50px;">MAKING  LIFE  <span class="text-primary">BETTER</span></h3>
                        <p style="text-align: center;">In 2019, Siva Yoga center is a the first and largest international yoga company to open in Viet Nam. With a mission of &quot;Making Life Better&quot; Siva yoga is not just another yoga center. It&#39;s a dynamic lifestyle center that aims
                            to inspire, entertain and energize the communities it serves.<br /><br /></p>
                        <div class="row">
                            <div class="col-xl-2" style="position: relative;"><img src="img/circle.png" style="width: 60px;height: 60px;" /><span class="text-main" style="position: absolute;right: 40px;top: 9px;font-size: 27px;">1</span></div>
                            <div class="col-xl-9">
                                <p style="text-align: left;margin: 0 auto;">Compleately pure air with the modern air filtration system, sterilizing and removing impurities.</p>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px;">
                            <div class="col-xl-2" style="position: relative;"><img src="img/circle.png" style="width: 60px;height: 60px;" /><span class="text-main" style="position: absolute;right: 40px;top: 9px;font-size: 27px;">2</span></div>
                            <div class="col-xl-9">
                                <p style="text-align: left;margin: 0 auto;">Meditative space with relaxing scent of argarwood from India </p>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px;">
                            <div class="col-xl-2" style="position: relative;"><img src="img/circle.png" style="width: 60px;height: 60px;" /><span class="text-main" style="position: absolute;right: 40px;top: 9px;font-size: 27px;">3</span></div>
                            <div class="col-xl-9">
                                <p style="text-align: left;margin: 0 auto;">Pure drink for Yoga people, especially nutritious grass juice</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       <section class="testimonials text-center bg-light">
            <div class="container">
                <h2 class="mb-5"><span class="text-primary">TALENT</span> MASTER</h2>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="mx-auto testimonial-item mb-5 mb-lg-0"><img class="rounded-circle img-fluid mb-3" src="img/vip.png" />
                            <h5>Standard</h5>
                            <p class="font-weight-light mb-0">Yoga and Therapy diploma of Uttarakhand Sanskrit University, India</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="mx-auto testimonial-item mb-5 mb-lg-0"><img class="rounded-circle img-fluid mb-3" src="masterKamal.jpg" style="height: 190px;" />
                            <h5>Premium</h5>
                            <p class="font-weight-light mb-0">Martial Artist, Doctor and Yoga Teacher from India.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="mx-auto testimonial-item mb-5 mb-lg-0"><img class="rounded-circle img-fluid mb-3" src="masterFemale.jpg" />
                            <h5>Vip</h5>
                            <p class="font-weight-light mb-0">Talent yoga teacher from the United states.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
    
    </form>

</body>
</html>
