﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Yoga_Management.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login page</title>
    <link href="css/bootstrap-bss.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
</head>
<body>
    <form id="formLogin" runat="server">
        <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9 col-lg-12 col-xl-10">
            <div class="card shadow-lg o-hidden border-0 my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-flex">
                            <img src="img/imgLogin.jpg" style="width: 100%;"/>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h4 class="text-dark mb-4">Welcome Back!</h4>
                                </div>
                               
                                    <div class="form-group">
                                         <asp:TextBox ID="txtEmail" CssClass="form-control form-control-user" placeholder="Enter Email Address..." TextMode="Email" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                         <asp:TextBox ID="txtPwd" CssClass="form-control form-control-user"  placeholder="Password" TextMode="Password" runat="server"></asp:TextBox>
                                    </div>
                                    
                               
                                <asp:Button ID="btnLogin" CssClass="btn btn-primary btn-block text-white btn-user" runat="server" Text="Login" OnClick="btnLogin_Click" />    
                                <hr />
                                

                                <div class="text-center"><a class="small" href="/Register.aspx">Create an Account!</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </form>

    <script src="js/bootstrap-bss.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/theme.js"></script>
</body>
</html>
