﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Yoga_Management._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

            <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
        <form class="form-inline d-none d-sm-inline-block mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group"><input type="text" class="bg-light form-control border-0 small" placeholder="Search for ..." />
                <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
            </div>
        </form>
        <ul class="nav navbar-nav flex-nowrap ml-auto">
            <li class="nav-item dropdown d-sm-none no-arrow"><a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#"><i class="fas fa-search"></i></a>
                <div role="menu" class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto navbar-search w-100">
                        <div class="input-group">
                            <input type="text" class="bg-light form-control border-0 small" placeholder="Search for ..." />
                            <div class="input-group-append">
                                <button class="btn btn-primary py-0" type="button">
                                    <i class="fas fa-search"></i></button></div>
                        </div>
                    </form>
                </div>
            </li>
            
           
            <div class="d-none d-sm-block topbar-divider"></div>
            <li role="presentation" class="nav-item dropdown no-arrow">
                <div class="nav-item dropdown no-arrow">
                    <a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#"> 
                    <span class="d-none d-lg-inline mr-2 text-gray-600 small">
                        <asp:Label ID="txtNameUser" runat="server" Text=""></asp:Label></span>
                    </a>
                    <div
                        role="menu" class="dropdown-menu shadow dropdown-menu-right animated--grow-in"><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-icon"></i> Profile</a><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-icon"></i> Cài đặt</a>
                        <div
                            class="dropdown-divider"></div>
                        
                        <a role="presentation" class="dropdown-item" href="#">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-icon"></i> 
                            <asp:Button ID="btnLogout" CssClass="btn btn-link" runat="server" Text="Log out" OnClick="fetchLogOut" /></a>

                    </div>
    </div>
    </li>
    </ul>
    </div>
</nav>
    <div class="container-fluid">
        <div class="d-sm-flex justify-content-between align-items-center mb-4">
            <h3 class="text-dark mb-0">Thống kê</h3>
        </div>
        <div class="row">
    <div class="col-md-6 col-xl-3 mb-4">
        <div class="card shadow py-2">
            <div class="card-body" style="padding-bottom: 10px!important;">
                <div class="row align-items-center no-gutters">
                    <div class="col mr-2">
                        <div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>SỐ LƯỢNG học viên</span></div>
                        <div class="text-dark font-weight-bold h5 mb-0"><span>
            <asp:Label ID="txtTongHocVien" runat="server" Text=""></asp:Label> </span><i class="fas fa-long-arrow-alt-up text-success" style="font-size: 15px;"></i><span style="font-size: 9px;"><asp:Label ID="txtSoLuongTang" runat="server" Text=""></asp:Label></span>
                        </div>
                    </div>
                    <div class="col-auto"><i class="fas fa-users fa-2x color-blur"></i></div>
                </div><span class="text-danger float-right" style="font-size: 10px;">học viên hết hạn hôm nay: <asp:Label ID="txtSoLuongGiam" runat="server" Text=""></asp:Label></span></div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3 mb-4">
        <div class="card shadow py-2">
            <div class="card-body">
                <div class="row align-items-center no-gutters">
                    <div class="col mr-2">
                        <div class="text-uppercase text-success font-weight-bold text-xs mb-1"></div>
                        <div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>SỐ LƯỢNG giáo viên</span></div>
                        <div class="text-dark font-weight-bold h5 mb-0"><span>
                            <asp:Label ID="txtSoLuongGV" runat="server" Text=""></asp:Label></span></div>
                    </div>
                    <div class="col-auto"><i class="fas fa-chalkboard-teacher fa-2x color-blur"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3 mb-4">
        <div class="card shadow py-2">
            <div class="card-body">
                <div class="row align-items-center no-gutters">
                    <div class="col mr-2">
                        <div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>doanh thu hôm nay</span></div>
                        <div class="text-dark font-weight-bold h5 mb-0"><span>
                                <asp:Label ID="txtDoanhThu" runat="server" Text=""></asp:Label>đ</span></div>
                    </div>
                    <div class="col-auto"><i class="fas fa-money-bill-wave fa-2x color-blur"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3 mb-4">
        <div class="card shadow py-2">
            <div class="card-body">
                <div class="row align-items-center no-gutters">
                    <div class="col mr-2">
                        <div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>DOANH THU THÁNG</span></div>
                        <div class="text-dark font-weight-bold h5 mb-0"><span>
                                    <asp:Label ID="txtDoanhThuThang" runat="server" Text="Label"></asp:Label>đ</span>
                            <i class="fas fa-long-arrow-alt-up text-success" style="font-size: 15px;"></i>
                            <span style="font-size: 9px;">15%</span>

                        </div>
                    </div>
                    <div class="col-auto"><i class="fas fa-money-check fa-2x color-blur"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>

</asp:Content>
