﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QLGoi.aspx.cs" Inherits="Yoga_Management.QLGoi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Package Management</title>
    <link href="css/bootstrap-bss.min.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap-bss.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="js/theme.js"></script>
</head>
<body>
        <div id="wrapper">
                <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion p-0" style="background-color: #6832a8;">
                    <div class="container-fluid d-flex flex-column p-0">
                        <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                            <div class="sidebar-brand-icon"><img src="img/boat.png" style="width: 100%;" /></div>
                            <div class="sidebar-brand-text mx-3"><span>SIVANA YOGA</span></div>
                        </a>
                        <hr class="sidebar-divider my-0" />
                        <ul class="nav navbar-nav text-light" id="accordionSidebar">
                            <li class="nav-item" role="presentation"><a class="nav-link" href="/"><i class="fas fa-signal"></i><span> Dashboard</span></a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="/QLHocVien"><i class="fas fa-users"></i><span> Student</span></a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="/QLGoi.aspx"><i class="far fa-newspaper"></i><span> Package</span></a></li>
                            <!--<li class="nav-item" role="presentation"><a class="nav-link" href="/QLGiaoVien.aspx"><i class="far fa-user"></i><span> Giáo viên</span></a></li>-->
                            <!--<li class="nav-item" role="presentation"><a class="nav-link" href="/QLCa.aspx"><i class="far fa-list-alt"></i><span> Ca</span></a></li>-->


                        </ul>
                        <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
                    </div>
                </nav>
            
            
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <form id="formQLCa" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                        <div class="container-fluid">
                            <button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button">
                                <i class="fas fa-bars"></i>

                            </button>
        
                                
        
                            <ul class="nav navbar-nav flex-nowrap ml-auto">                                                
                                <li role="presentation" class="nav-item dropdown no-arrow">
                                    <div class="nav-item dropdown no-arrow">
                                        <a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#"> 
                                        <span class="d-none d-lg-inline mr-2 text-gray-600 small">
                                            <asp:Label ID="txtNameUser" runat="server" Text=""></asp:Label></span>
                                        </a>
                                        <div
                                            role="menu" class="dropdown-menu shadow dropdown-menu-right animated--grow-in"><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-icon"></i> Profile</a><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-icon"></i> Cài đặt</a>
                                            <div
                                                class="dropdown-divider"></div>
                        
                                            <a role="presentation" class="dropdown-item" href="#">
                                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-icon"></i> 
                                                <asp:Button ID="btnLogout" CssClass="btn btn-link" runat="server" Text="Log out" OnClick="fetchLogOut" /></a>

                                        </div>
                        </div>
                        </li>
                        </ul>
                        </div>
                    </nav>
                    <div class="container">
                        <h5>Package management</h5>
                    </div>
                    
                    <div class="container d-flex mb-4">
                        <div>
                            <asp:LinkButton ID="btnAddPack" CssClass="btn btn-info btn-sm" UseSubmitBehavior="false" OnClick="openModalAdd" runat="server"> <i class="far fa-plus-square"></i></asp:LinkButton>
                            Add Package
                            <!--<asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="openModalAdd" CssClass="btn btn-info btn-sm" />-->
                            
                        </div>
                     </div>
                    <div>
                         <asp:GridView ID="goiGridView" style="margin: 0 auto;" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="G_ID" HeaderText="ID" />
                                <asp:BoundField DataField="G_TEN" HeaderText="Name" />
                                <asp:BoundField DataField="G_THOIHAN" HeaderText="Duration(month)" />
                                <asp:BoundField DataField="G_DONGIA" HeaderText="Price" DataFormatString="{0:###,###,###.00}VND" />


                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <!--<asp:Button ID="btnDel" CssClass="btn btn-danger btn-sm" OnClick="openModalDel" runat="server" Text="Del" />-->
                                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger btn-sm" UseSubmitBehavior="false" OnClick="openModalDel" runat="server">
                                            <i class="far fa-trash-alt"></i>
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-warning btn-sm" UseSubmitBehavior="false" OnClick="openModalEdit" runat="server">
                                            <i class="far fa-edit"></i>
                                        </asp:LinkButton>
                                        <!--<asp:Button ID="btnEdit" CssClass="btn btn-warning btn-sm" runat="server" Text="Edit" />-->
                                
                                        <!--<asp:Button ID="btnOrder" CssClass="btn btn-success btn-sm"  runat="server" Text="Invoice" />-->

                                    </ItemTemplate>
                            
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                            <SortedAscendingHeaderStyle BackColor="#848384" />
                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                            <SortedDescendingHeaderStyle BackColor="#575357" />
                        </asp:GridView>
                    </div>
                    

                    <div class="modal fade" id="modalDel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body text-center">
                                        <h6 class="text-danger">
                                            Do you want to delete this package with ID <strong><asp:Label ID="txtIDGT" runat="server" Text=""></asp:Label></strong>?
                                        </h6>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-link" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <asp:Button ID="btnSubmitDel" CssClass="btn btn-danger btn-sm" OnClick="fireDel" runat="server" Text="Delete" />
                                    </div>
                                </div>
                          
                      </div>
                    </div>

                    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtNameGT" CssClass="form-control form-control-user" placeholder="Enter Name" runat="server"></asp:TextBox>
                                            <div class="text-danger">
                                                <asp:Label ID="txtErrorEmail" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtDuration" CssClass="form-control form-control-user" placeholder="Duration" runat="server"></asp:TextBox>
                                            <div class="text-danger">
                                                <asp:Label ID="txtErrorPwd" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtMoney" placeholder="Price (VND)" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>
                                        </div>
                                        
                                    </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="modal-footer">
                                        <button class="btn btn-link" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <asp:Button ID="btnSave" CssClass="btn btn-success btn-sm" UseSubmitBehavior="false" OnClick="fireAdd" runat="server" Text="Save" />
                                        
                                    </div>
                                </div>
                          
                                </div>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        ID: <asp:Label ID="txtEditId" runat="server" Text=""></asp:Label>
                                        <asp:UpdatePanel runat="server">
                                            
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditNameGT" CssClass="form-control form-control-user" placeholder="Edit Name" runat="server"></asp:TextBox>
                                                    <div class="text-danger">
                                                        <asp:Label ID="Label1" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditDuration" CssClass="form-control form-control-user" MaxLength="10" placeholder="Duration" runat="server"></asp:TextBox>
                                                    <div class="text-danger">
                                                        <asp:Label ID="Label2" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditMoney" placeholder="Price" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>
                                                </div>
                    
                                        
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="modal-footer">
                                        <button class="btn btn-link" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <asp:Button ID="Button1" CssClass="btn btn-success btn-sm" UseSubmitBehavior="false" OnClick="fireEdit" runat="server" Text="Save" />
                                        
                                    </div>
                                </div>
                          
                                </div>
                        </div>
                    </div>


                </form>
               
                </div>
            </div>
         </div>
    
   
</body>
</html>
