﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Yoga_Management
{
    public partial class QLHocVien : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hocVienGridView.DataSource = null;
            hdGrid.DataSource = null;
            DataTable dSource = Load_HV();
            DataTable hdSource = Load_HD();

            hdGrid.DataSource = hdSource;
            hocVienGridView.DataSource = dSource;

            hdGrid.DataBind();
            hocVienGridView.DataBind();
            DateTime now = DateTime.Now;
            txtTimeStart.Text = now.ToString("yyyy-MM-dd");
            txtPwd.Attributes["value"] = txtPwd.Text;
        }
        private DataTable Load_HD()
        {
            string query = "SELECT *, HV_TEN FROM HOADON join HOCVIEN on HOCVIEN.HV_ID = HOADON.HV_ID";
            return clsDatabase.Element.Load(query);
        }
        private DataTable Load_HV()
        {
            string query = "SELECT *, G_TEN FROM HOCVIEN join GOITAP on HOCVIEN.G_ID = GOITAP.G_ID";
            return clsDatabase.Element.Load(query);
        }
        private string Load_GoiTapByName(string name)
        {
            string query = "SELECT * FROM GOITAP WHERE G_TEN='" + name + "';";
            DataTable data = clsDatabase.Element.Load(query);

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    if (row["G_DONGIA"].ToString() != "")
                    {
                        return row["G_DONGIA"].ToString();
                    }
                    
                }
            }
            return "";
        }
        private DataTable Load_GoiTap()
        {
            string query = "SELECT * FROM GOITAP";
            return clsDatabase.Element.Load(query);
        }
        protected void fetchLogOut(object sender, EventArgs e)
        {
            Session.Remove("role");
            Session.Remove("admin-id");
            Response.Redirect("/Login.aspx");
        }
        protected void testClick(object sender, EventArgs e)
        {
            Response.Write("<script>alert('welcome admin!')</script>");
        }
        protected void closeModalDel()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalDel", "$('#modalDel').modal('hide');", true);
            //upModal.Update();
        }
        protected void openModalAdd(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalAdd", "$('#modalAdd').modal();", true);


            goiTap.DataSource = Load_GoiTap();
            goiTap.DataTextField = "G_TEN";
            goiTap.DataValueField = "G_ID";
            goiTap.DataBind();
        }
        protected void openModalDel(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalDel", "$('#modalDel').modal();", true);

            //upModal.Update();
            LinkButton btn = (LinkButton)sender;
            //Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            modalDelName.Text = row.Cells[1].Text;
            modalDelID.Text = row.Cells[0].Text;
        }
        protected void openModalEdit(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalEdit", "$('#modalEdit').modal();", true);
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            txtEditId.Text = row.Cells[0].Text;
            txtEditName.Text = row.Cells[1].Text;
            txtEditEmail.Text = row.Cells[3].Text;
            txtEditPhone.Text = row.Cells[2].Text;
            txtEditTimeStart.Text = row.Cells[4].Text;
            txtEditTimeEnd.Text = row.Cells[5].Text;
            txtEditPack.Text = row.Cells[6].Text;
        }
        protected void openModalInvoice(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalInvoice", "$('#modalInvoice').modal();", true);
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            txtInvoiceId.Text = row.Cells[0].Text;
            txtInvoiceName.Text = row.Cells[1].Text;
            DateTime now = DateTime.Now;
            txtCreatedAt.Text = now.ToString("MM/dd/yyyy");
            txtMoney.Text = String.Format("{0:#,##0.00}", Math.Round(float.Parse(Load_GoiTapByName(row.Cells[6].Text)))); 
        }

        void LoadData()
        {
            try
            {
                DataTable dSource = Load_HV();

                hocVienGridView.DataSource = dSource;

                hocVienGridView.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        void LoadHD()
        {
            try
            {
                DataTable dSource = Load_HD();

                hdGrid.DataSource = dSource;

                hdGrid.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        protected void fireDel(object sender, EventArgs e)
        {
            try
            {
                string queryHD = "SELECT * FROM HOADON WHERE HV_ID = " + modalDelID.Text;
                DataTable HDData = clsDatabase.Element.Load(queryHD);
                if (HDData.Rows.Count > 0)
                {
                    foreach (DataRow row in HDData.Rows)
                    {
                        if (row["HV_ID"].ToString() != "")
                        {
                            string queryDel = "delete from HOADON where HV_ID =" + modalDelID.Text;
                            clsDatabase.Element.Execute(queryDel);
                        }
                    }
                }

                string query = "delete from HocVien where HV_ID =" + modalDelID.Text;
                clsDatabase.Element.Execute(query);

                Response.Write("<script>alert('success')</script>");
                LoadData();
                LoadHD();
                closeModalDel();
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnClick(object sender, EventArgs e)
        {
            Response.Write("<script>alert('welcome admin!')</script>");
        }
        protected void hocVienGridView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected DateTime next(DateTime date)
        {
            if (date.Day != DateTime.DaysInMonth(date.Year, date.Month))
                return date.AddMonths(1);
            else
                return date.AddDays(1).AddMonths(1).AddDays(-1);
        }
        private bool isNullData(DataTable data)
        {
            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    if (row["HV_ID"].ToString() != "")
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        protected void fireAdd(object sender, EventArgs e)
        {
            try
            {
                string queryUser = "SELECT * FROM HOCVIEN WHERE HV_UNAME = '" + txtEmail.Text.Trim() + "'";
                DataTable userData = clsDatabase.Element.Load(queryUser);

                if (!isNullData(userData))
                {
                    txtEmail.CssClass += " border-danger";
                    txtErrorEmail.Text = "This email is existed, please try another email!";
                    return;
                }
                string query = "INSERT INTO HOCVIEN(HV_TEN, HV_SDT, HV_UNAME, HV_PWD, G_ID, HV_NGAYBATDAU, HV_NGAYKETTHUC) values  (N'" + txtName.Text +
                                "','" + txtSDT.Text + "','" + txtEmail.Text + "','" + txtPwd.Text + "','" + goiTap.SelectedItem.Value + "','"
                                + txtTimeStart.Text + "','" + txtTimeEnd.Text + "')";


                clsDatabase.Element.Execute(query);
                LoadData();
                Response.Write("<script>alert('Success!')</script>");
                //Response.Redirect("/Login.aspx");
            }
            catch (Exception ex)
            {

            }
            
        }
        protected string getQueryUpdate()
        {
            string queryUpdate1 = "UPDATE HOCVIEN SET HV_TEN=N'" + txtEditName.Text + "', HV_SDT='" + txtEditPhone.Text + "' WHERE HV_ID = '" + txtEditId.Text.Trim() + "'";
            string queryUpdate2 = "UPDATE HOCVIEN SET HV_TEN=N'" + txtEditName.Text + "', HV_SDT='" + txtEditPhone.Text + "', HV_PWD = '" + txtEditPwd.Text + "' WHERE HV_ID = '" + txtEditId.Text.Trim() + "'";
            if (txtEditPwd.Text != "")
            {
                return queryUpdate2;
            }
            return queryUpdate1;
        }
        protected void fireEdit(object sender, EventArgs e)
        {
            try
            {
                string query = getQueryUpdate();
                clsDatabase.Element.Execute(query);
                LoadData();
                Response.Write("<script>alert('Success!')</script>");
                //Response.Redirect("/Login.aspx");
            }
            catch (Exception ex)
            {

            }

        }
        private bool isNullInvoice(DataTable data)
        {
            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    if (row["HD_MA"].ToString() != "")
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        protected void fireCreateInvoice(object sender, EventArgs e)
        {
            try
            {
                string queryInvoice = "SELECT * FROM HOADON WHERE HV_ID = '" + txtInvoiceId.Text.Trim() + "'";
                DataTable invoiceData = clsDatabase.Element.Load(queryInvoice);
                if (!isNullInvoice(invoiceData))
                {
                    Response.Write("<script>alert('This user already had Invoice!')</script>");
                    return;
                }
                string queryInsert = "insert into HOADON(HV_ID, HD_TENKHACHHANG, HD_NGAYTAO, HD_THANHTIEN, HD_NOTE) values('" + txtInvoiceId.Text + "','" +
                    txtInvoiceName.Text + "','" + txtCreatedAt.Text + "','" + float.Parse(txtMoney.Text).ToString() + "','" + txtNote.Text + "');";
                clsDatabase.Element.Execute(queryInsert);
                LoadHD();
                Response.Write("<script>alert('Success!')</script>");

            } catch (Exception ex)
            {

            }
        }
        protected void goiTap_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (goiTap.SelectedItem.Value == "1")
            {
                DateTime now = DateTime.Now;
                DateTime nextMonth = next(now);
                txtTimeEnd.Text = nextMonth.ToString("yyyy-MM-dd");
            }else if(goiTap.SelectedItem.Value == "2")
            {
                txtTimeEnd.Text = DateTime.Now.AddMonths(2).ToString("yyyy-MM-dd");
            }else if(goiTap.SelectedItem.Value == "3")
            {
                txtTimeEnd.Text = DateTime.Now.AddMonths(3).ToString("yyyy-MM-dd");
            }
        }
    }
}