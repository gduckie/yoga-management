﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QLThoiKhoaBieu.aspx.cs" Inherits="Yoga_Management.QLThoiKhoaBieu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/bootstrap-bss.min.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
            <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion p-0" style="background-color: #6832a8;">
            <div class="container-fluid d-flex flex-column p-0">
                <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon"><img src="img/boat.png" style="width: 100%;" /></div>
                    <div class="sidebar-brand-text mx-3"><span>SIVANA YOGA</span></div>
                </a>
                <hr class="sidebar-divider my-0" />
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="/"><i class="fas fa-signal"></i><span> Thống kê</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/QLHocVien"><i class="fas fa-users"></i><span> Học viên</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/QLMonHoc.aspx"><i class="far fa-newspaper"></i><span> Môn học</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/QLGiaoVien.aspx"><i class="far fa-user"></i><span> Giáo viên</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/QLCa.aspx"><i class="far fa-list-alt"></i><span> Ca</span></a></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
            
            
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <form id="formQLThoiKhoaBieu" runat="server">
        
        <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
        
            <div class="input-group"><input type="text" class="bg-light form-control border-0 small" placeholder="Search for ..." />
                <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
            </div>
        
        <ul class="nav navbar-nav flex-nowrap ml-auto">
            <li class="nav-item dropdown d-sm-none no-arrow"><a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#"><i class="fas fa-search"></i></a>
                <div role="menu" class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto navbar-search w-100">
                        <div class="input-group">
                            <input type="text" class="bg-light form-control border-0 small" placeholder="Search for ..." />
                            <div class="input-group-append">
                                <button class="btn btn-primary py-0" type="button">
                                    <i class="fas fa-search"></i></button></div>
                        </div>
                    </form>
                </div>
            </li>
            
           
            <div class="d-none d-sm-block topbar-divider"></div>
            <li role="presentation" class="nav-item dropdown no-arrow">
                <div class="nav-item dropdown no-arrow">
                    <a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#"> 
                    <span class="d-none d-lg-inline mr-2 text-gray-600 small">
                        <asp:Label ID="txtNameUser" runat="server" Text=""></asp:Label></span>
                    </a>
                    <div
                        role="menu" class="dropdown-menu shadow dropdown-menu-right animated--grow-in"><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-icon"></i> Profile</a><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-icon"></i> Cài đặt</a>
                        <div
                            class="dropdown-divider"></div>
                        
                        <a role="presentation" class="dropdown-item" href="#">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-icon"></i> 
                            <asp:Button ID="btnLogout" CssClass="btn btn-link" runat="server" Text="Log out" OnClick="fetchLogOut" /></a>

                    </div>
    </div>
    </li>
    </ul>
    </div>
</nav>
            
                QL TKB
            
            </form>
                
                <hr />
                <footer>
                    <p>&copy; <%: DateTime.Now.Year %> - Yoga Sivana</p>
                </footer>
            </div>
        </div>
         </div>
    
    <script src="js/bootstrap-bss.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/theme.js"></script>
    
</body>
</html>
