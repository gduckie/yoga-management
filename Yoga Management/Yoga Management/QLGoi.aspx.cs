﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Yoga_Management
{
    public partial class QLGoi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            goiGridView.DataSource = null;
            DataTable dSource = Load_GT();
            goiGridView.DataSource = dSource;
            goiGridView.DataBind();
        }
        private DataTable Load_GT()
        {
            string query = "SELECT * FROM GOITAP";
            return clsDatabase.Element.Load(query);
        }
        void LoadData()
        {
            try
            {
                DataTable dSource = Load_GT();

                goiGridView.DataSource = dSource;

                goiGridView.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        protected void fetchLogOut(object sender, EventArgs e)
        {
            Session.Remove("role");
            Session.Remove("admin-id");
            Response.Redirect("/Login.aspx");
        }
        protected void openModalAdd(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalAdd", "$('#modalAdd').modal();", true);
        }

        protected void fireAdd(object sender, EventArgs e)
        {
            try
            {
                string query = "insert into GOITAP(G_TEN, G_THOIHAN, G_DONGIA) values('" + txtNameGT.Text + "', '" + txtDuration.Text + "','" + txtMoney.Text + "');";
                clsDatabase.Element.Execute(query);
                LoadData();
                Response.Write("<script>alert('Success!')</script>");
                //Response.Redirect("/Login.aspx");
            }
            catch (Exception ex)
            {

            }

        }

        protected void openModalDel(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalDel", "$('#modalDel').modal();", true);

            //upModal.Update();
            LinkButton btn = (LinkButton)sender;
            //Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            //modalDelName.Text = row.Cells[1].Text;
            txtIDGT.Text = row.Cells[0].Text;
        }
        protected void openModalEdit(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalEdit", "$('#modalEdit').modal();", true);
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
            txtEditId.Text = row.Cells[0].Text;
            txtEditNameGT.Text = row.Cells[1].Text;
            txtEditDuration.Text = row.Cells[2].Text;
            //txtEditMoney.Text = double.Parse(row.Cells[3].Text).ToString("#,###", cul.NumberFormat);
            txtEditMoney.Text = double.Parse(Regex.Replace(row.Cells[3].Text, @"[^\d.]", "")).ToString();
        }

        protected void fireDel(object sender, EventArgs e)
        {
            try
            {
                string query = "delete from GOITAP where G_ID ='" + txtIDGT.Text + "';";
                clsDatabase.Element.Execute(query);

                Response.Write("<script>alert('success')</script>");
                LoadData();

            }
            catch (Exception ex)
            {

            }
        }

        protected void fireEdit(object sender, EventArgs e)
        {
            try
            {
                string query = "UPDATE GOITAP SET G_TEN = '" + txtEditNameGT.Text + "', G_THOIHAN= '" + txtEditDuration.Text + "', G_DONGIA= '" + txtEditMoney.Text +"' WHERE G_ID = '" + txtEditId.Text + "';";
                clsDatabase.Element.Execute(query);
                LoadData();
                Response.Write("<script>alert('Success!')</script>");
                //Response.Redirect("/Login.aspx");
            }
            catch (Exception ex)
            {

            }

        }
    }
}