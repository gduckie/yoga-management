﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Yoga_Management.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register page</title>
    <link href="css/bootstrap-bss.min.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
</head>
<body>
    <form id="formRegister" runat="server">
        <div class="container">
    <div class="card shadow-lg o-hidden border-0 my-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-lg-6 d-none d-lg-flex">
                    <img src="img/imgHome.jpg" style="width: 100%;"/>
                    
                </div>
                <div class="col-lg-6">
                    <div class="p-5">
                        <div class="text-center">
                            <h4 class="text-dark mb-4">Create an Account!</h4>
                        </div>
                        
                            
                            <div class="form-group">
                                <asp:TextBox ID="txtEmail" CssClass="form-control form-control-user" TextMode="Email" placeholder="Email Address" runat="server"></asp:TextBox>
                                <div class="text-danger">
                                    <asp:Label ID="txtErrorEmail" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                  </div>
                            </div>
                        <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <asp:TextBox ID="txtPwd" CssClass="form-control form-control-user" MaxLength="10" TextMode="Password" placeholder="Password" runat="server"></asp:TextBox>
                                    <div class="text-danger">
                                    <asp:Label ID="txtErrorPwd" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtRePwd" CssClass="form-control form-control-user" MaxLength="10" TextMode="Password"  placeholder="Repeat Password" runat="server"></asp:TextBox>
                                    <div class="text-danger">
                                    <asp:Label ID="txtErrorRePwd" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtName" placeholder="Name" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtNamSinh" placeholder="Name" runat="server" CssClass="form-control form-control-user"  TextMode="Date"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtSDT" placeholder="Phone number" MaxLength="11" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>
                                <div class="text-danger">
                                <asp:RegularExpressionValidator CssClass="errorStyle" ID="errorPhone" runat="server" ErrorMessage="Enter valid Phone number" ControlToValidate="txtSDT" ValidationExpression="^[0-9]*$" ></asp:RegularExpressionValidator> 
                                </div>
                            </div>

                            
                            <asp:Button ID="btnRegister" CssClass="btn btn-primary btn-block text-white btn-user" runat="server" Text="Register Account" OnClick="btnRegister_Click" />
                            
                            <hr />
                       
                        <div class="text-center"><a class="small" href="/Login.aspx">Already have an account? Login!</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
     <script src="js/bootstrap-bss.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/theme.js"></script>
    </form>
     </body>
</html>
