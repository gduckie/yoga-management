﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Yoga_Management
{
    public class clsDatabase
    {
        public static SqlConnection con;
        private static clsDatabase element = null;
        private SqlDataAdapter adapter;
        private SqlCommand command;
        private SqlConnection connection;
        private clsDatabase()
        {
            string str = "Data Source=.\\SQLEXPRESS;Initial Catalog=yogaManagement;User ID=mylogin;Password=mylogin";
            this.connection = new SqlConnection(str);
        }
        public static clsDatabase Element
        {
            get
            {
                if (element == null)
                {
                    element = new clsDatabase();
                }
                return element;
            }
        }
        public DataTable Load(string sql)
        {
            DataTable tb = new DataTable();
            this.adapter = new SqlDataAdapter(sql, this.connection);
            this.adapter.Fill(tb);
            return tb;
        }
        public int Execute(string sql)
        {
            SqlCommand command = new SqlCommand(sql, this.connection);
            this.connection.Open();
            int rs = command.ExecuteNonQuery();
            this.connection.Close();
            return rs;
        }
        public List<string> Read(string sql)
        {
            SqlDataReader reader;
            List<string> data = new List<string>();
            command = new SqlCommand(sql, this.connection);
            this.connection.Open();

            reader = command.ExecuteReader();
            while (reader.Read())
            {
                data.Add(reader["name"].ToString());
            }
            this.connection.Close();

            return data;
        }
        public static bool OpenConnection()
        {
            try
            {
                con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=SinhVien;Persist Security Info=True;User ID=mylogin;Password=mylogin");
                con.Open();
            }
            catch
            {
                return false;
            }
            return true;
        }
        public static bool CloseConnection()
        {
            try
            {
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}