﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Yoga_Management
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        private bool isNullData(DataTable data, string type)
        {
            if(type == "admin")
            {
                if(data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        if(row["AD_ID"].ToString() != "")
                        {
                            return false;
                        }
                    }
                }
            }
            else if(type == "user")
            {
                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        if (row["HV_ID"].ToString() != "")
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        private void FetchLogin()
        {
            string queryAdmin = "SELECT * FROM ADMINISTRATOR WHERE AD_UNAME = '" + txtEmail.Text.Trim() + "' AND AD_PWD = '" + txtPwd.Text.Trim() + "'";
            DataTable adminData = clsDatabase.Element.Load(queryAdmin);

            string queryUser = "SELECT * FROM HOCVIEN WHERE HV_UNAME = '" + txtEmail.Text.Trim() + "' AND HV_PWD = '" + txtPwd.Text.Trim() + "'";
            DataTable userData = clsDatabase.Element.Load(queryUser);
           
            if (!isNullData(adminData, "admin") && isNullData(userData, "user"))
            {
                if (adminData.Rows.Count > 0)
                {
                    foreach (DataRow row in adminData.Rows)
                    {
                        Session["role"] = "admin";
                        Session["admin-id"] = row["AD_ID"].ToString();
                        Session["uname"] = row["AD_UNAME"].ToString();
                        Response.Write("<script>alert('welcome admin!')</script>");
                        Response.Redirect("/");
                    }
                }
            }
            
            else if(isNullData(adminData, "admin") && !isNullData(userData, "user"))
            {
                if (userData.Rows.Count > 0)
                {
                    foreach (DataRow row in userData.Rows)
                    {
                        Session["role"] = "user";
                        Session["user-id"] = row["HV_ID"].ToString();
                        Session["uname"] = row["HV_UNAME"].ToString();
                        Response.Write("<script>alert('welcome user!')</script>");
                        Response.Redirect("/User");
                    }
                }
            }
            else
            {
                Response.Write("<script>alert('Wrong account or password!')</script>");
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            FetchLogin();
        }
    }
}