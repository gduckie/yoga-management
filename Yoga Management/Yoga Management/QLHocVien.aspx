﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QLHocVien.aspx.cs" Inherits="Yoga_Management.QLHocVien" EnableEventValidation="false"  MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Guest Management</title>
    <link href="css/bootstrap-bss.min.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
     <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap-bss.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="js/theme.js"></script>
</head>
<body>
    <div id="wrapper">
                <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion p-0" style="background-color: #6832a8;">
                <div class="container-fluid d-flex flex-column p-0">
                    <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                        <div class="sidebar-brand-icon"><img src="img/boat.png" style="width: 100%;" /></div>
                        <div class="sidebar-brand-text mx-3"><span>SIVANA YOGA</span></div>
                    </a>
                    <hr class="sidebar-divider my-0" />
                    <ul class="nav navbar-nav text-light" id="accordionSidebar">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/"><i class="fas fa-signal"></i><span> Dashboard</span></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/QLHocVien"><i class="fas fa-users"></i><span> Student</span></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/QLGoi.aspx"><i class="far fa-newspaper"></i><span> Package</span></a></li>
                        <!--<li class="nav-item" role="presentation"><a class="nav-link" href="/QLGiaoVien.aspx"><i class="far fa-user"></i><span> Giáo viên</span></a></li>-->
                        <!--<li class="nav-item" role="presentation"><a class="nav-link" href="/QLCa.aspx"><i class="far fa-list-alt"></i><span> Ca</span></a></li>-->


                    </ul>
                    <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
                </div>
            </nav>
            
            
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <form id="formQLHocVien" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                        <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
        
                                <div class="input-group"><input type="text" class="bg-light form-control border-0 small" placeholder="Search for ..." />
                                    <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                </div>
        
                            <ul class="nav navbar-nav flex-nowrap ml-auto">
                                <li class="nav-item dropdown d-sm-none no-arrow">
                                    <a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#">
                                        <i class="fas fa-search"></i></a>
                                    <div role="menu" class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" aria-labelledby="searchDropdown">
                    
                                            <div class="input-group">
                                                <input type="text" class="bg-light form-control border-0 small" placeholder="Search for ..." />
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary py-0" type="button">
                                                        <i class="fas fa-search"></i></button></div>
                                            </div>
                 
                                    </div>
                                </li>
            
           
                            
                                <li role="presentation" class="nav-item dropdown no-arrow">
                                    <div class="nav-item dropdown no-arrow">
                                        <a data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle nav-link" href="#"> 
                                        <span class="d-none d-lg-inline mr-2 text-gray-600 small">
                                            <asp:Label ID="txtNameUser" runat="server" Text=""></asp:Label></span>
                                        </a>
                                        <div
                                            role="menu" class="dropdown-menu shadow dropdown-menu-right animated--grow-in"><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-icon"></i> Profile</a><a role="presentation" class="dropdown-item" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-icon"></i> Cài đặt</a>
                                            <div
                                                class="dropdown-divider"></div>
                        
                                            <a role="presentation" class="dropdown-item" href="#">
                                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-icon"></i> 
                                                <asp:Button ID="btnLogout" CssClass="btn btn-link" runat="server" Text="Log out" OnClick="fetchLogOut" /></a>

                                        </div>
                        </div>
                        </li>
                        </ul>
                        </div>
                    </nav>
                    <div class="container">
                        <h5>Student management</h5>
                    </div>
                    
                    <div class="container d-flex mb-4">
                        <div>
                            <asp:LinkButton ID="btnAddUser" CssClass="btn btn-info btn-sm" UseSubmitBehavior="false" OnClick="openModalAdd" runat="server">
                                <i class="fas fa-user-plus"></i>
                            </asp:LinkButton>
                            Add Student
                            <!--<asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="openModalAdd" CssClass="btn btn-info btn-sm" />-->
                            
                        </div>
                     </div>
                    <div>
                         <asp:GridView ID="hocVienGridView" style="margin: 0 auto;" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" OnSelectedIndexChanged="hocVienGridView_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="HV_ID" HeaderText="ID" />
                        <asp:BoundField DataField="HV_TEN" HeaderText="Full Name" />
                        <asp:BoundField DataField="HV_SDT" HeaderText="Phone Number" />
                        <asp:BoundField DataField="HV_UNAME" HeaderText="Email" />
                        <asp:BoundField DataField="HV_NGAYBATDAU" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Day Start" />
                        <asp:BoundField DataField="HV_NGAYKETTHUC" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Day End" />
                        <asp:BoundField DataField="G_TEN" HeaderText="Pack" />
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <!--<asp:Button ID="btnDel" CssClass="btn btn-danger btn-sm" OnClick="openModalDel" runat="server" Text="Del" />-->
                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-danger btn-sm" UseSubmitBehavior="false" OnClick="openModalDel" runat="server">
                                    <i class="fas fa-user-times"></i>
                                </asp:LinkButton>

                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-warning btn-sm" UseSubmitBehavior="false" OnClick="openModalEdit" runat="server">
                                    <i class="fas fa-user-edit"></i>
                                </asp:LinkButton>
                                <!--<asp:Button ID="btnEdit" CssClass="btn btn-warning btn-sm" runat="server" Text="Edit" />-->
                                
                                <!--<asp:Button ID="btnOrder" CssClass="btn btn-success btn-sm"  runat="server" Text="Invoice" />-->
                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-success btn-sm" UseSubmitBehavior="false" OnClick="openModalInvoice" runat="server">
                                    <i class="fas fa-file-invoice"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                    <SortedAscendingHeaderStyle BackColor="#848384" />
                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                    <SortedDescendingHeaderStyle BackColor="#575357" />
            </asp:GridView>
                    </div>

                    <div class="container">
                        <h5>Invoice</h5>
                    </div>
                    <div class="container mt-3" >
                       
                        
                
                        <asp:GridView ID="hdGrid" style="margin-left: 9.5rem;" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="HD_MA" HeaderText="ID" />
                                <asp:BoundField DataField="HV_TEN" HeaderText="Name Student" />
                                <asp:BoundField DataField="HD_THANHTIEN" HeaderText="Price(VND)" DataFormatString="{0:###,###,###.00}VND" />
                                <asp:BoundField DataField="HD_NOTE" HeaderText="Note" />
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        </asp:GridView>
                    </div>
                    

                    <div class="modal fade" id="modalDel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body text-center">
                                        <h6 class="text-danger">Do you want to delete user <strong><asp:Label ID="modalDelName" runat="server" Text=""></asp:Label></strong>
                                            with ID: <strong><asp:Label ID="modalDelID" runat="server" Text=""></asp:Label></strong>
                                        </h6>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-link" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <asp:Button ID="btnSubmitDel" CssClass="btn btn-danger btn-sm" OnClick="fireDel" runat="server" Text="Delete" />
                                    </div>
                                </div>
                          
                      </div>
                    </div>

                    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtEmail" CssClass="form-control form-control-user" TextMode="Email" placeholder="Email Address" runat="server"></asp:TextBox>
                                            <div class="text-danger">
                                                <asp:Label ID="txtErrorEmail" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtPwd" CssClass="form-control form-control-user" MaxLength="10" TextMode="Password" placeholder="Password" runat="server"></asp:TextBox>
                                            <div class="text-danger">
                                                <asp:Label ID="txtErrorPwd" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtName" placeholder="Name" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:DropDownList ID="goiTap" AutoPostBack="true" CssClass="form-control form-control-user" runat="server" OnSelectedIndexChanged="goiTap_SelectedIndexChanged">
                                                <asp:ListItem Text ="Package" Value = "package" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <div class="form-group">
                                            <label class="text-dark" style="font-weight: 600; font-size: 13px;">Day Start</label>
                                            <asp:TextBox ID="txtTimeStart" placeholder="Name" runat="server" CssClass="form-control form-control-user"  TextMode="Date"></asp:TextBox>
                                        </div>

                                        <div class="form-group">
                                            <label class="text-dark" style="font-weight: 600; font-size: 13px;">Day Start</label>
                                            <asp:TextBox ID="txtTimeEnd" placeholder="Name" runat="server" CssClass="form-control form-control-user"  TextMode="Date"></asp:TextBox>
                                        </div>
                                          
                                        <div class="form-group">
                                            <asp:TextBox ID="txtSDT" placeholder="Phone number" MaxLength="11" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>

                                        </div>  
                                        
                                    </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="modal-footer">
                                        <button class="btn btn-link" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <asp:Button ID="btnSave" CssClass="btn btn-success btn-sm" UseSubmitBehavior="false" OnClick="fireAdd" runat="server" Text="Save" />
                                        
                                    </div>
                                </div>
                          
                                </div>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        ID: <asp:Label ID="txtEditId" runat="server" Text=""></asp:Label>
                                        <asp:UpdatePanel runat="server">
                                            
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditEmail" CssClass="form-control form-control-user" ReadOnly="True" TextMode="Email" placeholder="Email Address" runat="server"></asp:TextBox>
                                                    <div class="text-danger">
                                                        <asp:Label ID="Label1" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditPwd" CssClass="form-control form-control-user" MaxLength="10" TextMode="Password" placeholder="Password" runat="server"></asp:TextBox>
                                                    <div class="text-danger">
                                                        <asp:Label ID="Label2" CssClass="errorStyle" runat="server" Text="" ></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditName" placeholder="Name" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditPack" placeholder="Package" ReadOnly="True" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>
                                            

                                                </div>
                                                <div class="form-group">
                                                    <label class="text-dark" style="font-weight: 600; font-size: 13px;">Day Start</label>
                                                    <asp:TextBox ID="txtEditTimeStart" ReadOnly="True" runat="server" CssClass="form-control form-control-user"></asp:TextBox>
                                                </div>

                                                <div class="form-group">
                                                    <label class="text-dark" style="font-weight: 600; font-size: 13px;">Day Start</label>
                                                    <asp:TextBox ID="txtEditTimeEnd" ReadOnly="True" runat="server" CssClass="form-control form-control-user"></asp:TextBox>
                                                </div>
                                          
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtEditPhone" placeholder="Phone number" MaxLength="11" runat="server" CssClass="form-control form-control-user" ></asp:TextBox>

                                                </div>  
                                        
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="modal-footer">
                                        <button class="btn btn-link" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <asp:Button ID="Button1" CssClass="btn btn-success btn-sm" UseSubmitBehavior="false" OnClick="fireEdit" runat="server" Text="Save" />
                                        
                                    </div>
                                </div>
                          
                                </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modalInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        ID: <asp:Label ID="txtInvoiceId" style="font-weight: 600;" runat="server" Text=""></asp:Label>
                                        <asp:UpdatePanel runat="server">
                                            
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <label style="font-weight: 600;">Name: </label>
                                                    <asp:Label ID="txtInvoiceName" runat="server" Text="Label"></asp:Label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label style="font-weight: 600;">Day: </label>
                                                    <asp:Label ID="txtCreatedAt" runat="server" Text=""></asp:Label>
                                                </div>
                                                <div class="form-group">
                                                    <label style="font-weight: 600;">Price: </label>
                                                    <asp:Label ID="txtMoney" runat="server" Text=""></asp:Label> VND
                                            

                                                </div>
                                                <div class="form-group">
                                                    <label class="text-dark" style="font-weight: 600; font-size: 13px;">Note</label>
                                                    <asp:TextBox ID="txtNote" runat="server" CssClass="form-control form-control-user"></asp:TextBox>
                                                </div>
                                                                                   
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="modal-footer">
                                        <button class="btn btn-link" data-dismiss="modal" aria-hidden="true">Close</button>
                                        <asp:Button ID="btnCreateInvoice" CssClass="btn btn-success btn-sm" UseSubmitBehavior="false" OnClick="fireCreateInvoice" runat="server" Text="Save" />
                                        
                                    </div>
                                </div>
                          
                                </div>
                        </div>
                    </div>


                </form>
                
                    <hr />
                </div>
            </div>
         </div>
    
   
</body>
</html>
