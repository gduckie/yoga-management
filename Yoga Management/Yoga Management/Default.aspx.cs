﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Yoga_Management
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["role"] == null || Session["role"].ToString() != "" && Session["role"].ToString() != "admin")
            {
                Session.Remove("role");
                Session.Remove("user-id");
                Response.Redirect("/Login.aspx");
            }
            txtNameUser.Text = Session["uname"].ToString();
            initData();
        }
        private void initData()
        {
            //so luong hoc vien
            string query1 = "select COUNT(HV_ID)AS amount FROM HOCVIEN";
            DataTable soluongHV = clsDatabase.Element.Load(query1);
            if (soluongHV.Rows.Count > 0)
            {
                foreach (DataRow row in soluongHV.Rows)
                {
                    txtTongHocVien.Text = row["amount"].ToString();
                }
            }

            //so luong hoc vien tang hom nay
            string query2 = "select COUNT(HV_ID) AS AMOUNT from HOCVIEN WHERE HV_NGAYBATDAU = CONVERT(VARCHAR(10), getdate(), 111);";
            DataTable soluongHVTang = clsDatabase.Element.Load(query2);
            if (soluongHVTang.Rows.Count > 0)
            {
                foreach (DataRow row in soluongHVTang.Rows)
                {
                    txtSoLuongTang.Text = row["amount"].ToString();
                }
            }

            //so luon hoc vien het han hom nay
            string query3 = "select COUNT(HV_ID) AS AMOUNT from HOCVIEN WHERE HV_NGAYKETTHUC = CONVERT(VARCHAR(10), getdate(), 111);";
            DataTable soluongHVGiam = clsDatabase.Element.Load(query3);
            if (soluongHVGiam.Rows.Count > 0)
            {
                foreach (DataRow row in soluongHVGiam.Rows)
                {
                    txtSoLuongGiam.Text = row["amount"].ToString();
                }
            }

            //so luong giao vien
            string query4 = "select COUNT(gv_id) as amount from GIAOVIEN;";
            DataTable soLuongGV = clsDatabase.Element.Load(query4);
            if (soLuongGV.Rows.Count > 0)
            {
                foreach (DataRow row in soLuongGV.Rows)
                {
                    txtSoLuongGV.Text = row["amount"].ToString();
                }
            }

            //Doanh thu hom nay
            string query5 = "select SUM(hd_thanhtien) as amount from HOADON WHERE HD_NGAYTAO = CONVERT(VARCHAR(10), getdate(), 111)";
            DataTable doanhThu = clsDatabase.Element.Load(query5);
            if (doanhThu.Rows.Count > 0)
            {
                foreach (DataRow row in doanhThu.Rows)
                {
                    if (row["amount"].ToString() != "")
                    {
                        txtDoanhThu.Text = String.Format("{0:#,##0.00}", Math.Round(float.Parse(row["amount"].ToString())));
                    }
                    else
                    {
                        txtDoanhThu.Text = "0";
                    }
                    
                    //txtDoanhThu.Text = row["amount"].ToString();
                }
            }

            //Doanh thu thang
            string query6 = "select SUM(hd_thanhtien) as amount from HOADON WHERE month(HD_NGAYTAO) = MONTH(getdate());";
            DataTable doanhthuThang = clsDatabase.Element.Load(query6);
            if (doanhthuThang.Rows.Count > 0)
            {
                foreach (DataRow row in doanhthuThang.Rows)
                {
                    txtDoanhThuThang.Text = String.Format("{0:#,##0.00}", Math.Round(float.Parse(row["amount"].ToString())));
                }
            }
        }
        protected void fetchLogOut(object sender, EventArgs e)
        {
            Session.Remove("role");
            Session.Remove("admin-id");
            Response.Redirect("/Login.aspx");
        }
    }
}